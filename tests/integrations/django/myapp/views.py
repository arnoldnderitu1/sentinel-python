from django.http import HttpResponse
from django.template import Template, Context

import sentinel_sdk


def self_check(request):
    with sentinel_sdk.configure_scope() as scope:
        assert scope._data["transaction"] == "self_check"
    return HttpResponse("ok")


def view_exc(request):
    1 / 0


def get_dsn(request):
    template = Template("{% load sentinel %}{% sentinel_dsn %}!")
    return HttpResponse(
        template.render(Context()), content_type="application/xhtml+xml"
    )


def message(request):
    sentinel_sdk.capture_message("hi")
    return HttpResponse("ok")
