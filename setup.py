#!usr/bin/env

"""
Sentinel-Python - Sentinel SDK for Python
=========================================

**Sentinel-Python is an SDK for Sentinel.** Checkout out `Gitlab
<https://gitlab.com/arnoldnderitu1/sentinel-python>`_ to find out more.
"""

from setuptools import setup, find_packages


setup(
    name="Sentinel-sdk",
    version="0.1",
    author="Arnold Nderitu",
    author_email="arnoldnderitu@gmail.com",
    url="https://gitlab.com/arnoldnderitu1/sentinel-python",
    description="Python client for Sentinel",
    packages=find_packages(exclude=("tests", "tests.*")),
    zip_safe=False,
    license="BSD",
    install_requires=["urllib3", "certifi"],
    extras_require={"flask": ["flask>=0.8", "blinker>=1.1"]},
)
